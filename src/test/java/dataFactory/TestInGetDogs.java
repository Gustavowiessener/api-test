package dataFactory;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;

public class TestInGetDogs {

    public static String token = "81687091-6c9b-4917-a11b-5bafff6bf156";
    public static String temperament = "Alert, Intelligent, Faithful, Active, Instinctual, Trainable";
    public static String idsAsserts = "[195, 196, 197, 249]";
    public static String nameAsserts = "[Poodle, Poodle (Miniature), Poodle (Toy), Toy Poodle]";

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = "https://api.thedogapi.com/v1";
    }

    @Test
    @DisplayName("Simulação de todas as raças da Api deve retornar Http Status 200 e listagem completa")
    public void TestInGetAllBreeds() {
        Response response = given()
                .header("x-api-key", token)
                .contentType(ContentType.JSON)
                .when()
                .get("/breeds")
                .then()
                .log().all()
                .extract().response();


        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals(true, response.jsonPath().getString("id").contains("264"));
    }

    @Test
    @DisplayName("A simulação para filtrar as raças com parametros, retorna HTTP status 200 e informações no body")
    public void TestInGetBreedsSearch() {
        Response response = given()
                .header("x-api-key", token)
                .contentType(ContentType.JSON)
                .param("q", "poodle")
                .when()
                .get("/breeds/search")
                .then()
                .log().all()
                .extract().response();


        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals(idsAsserts, response.jsonPath().getString("id"));
        Assertions.assertEquals(nameAsserts, response.jsonPath().getString("name"));
        Assertions.assertEquals(temperament, response.jsonPath().getString("temperament"));
    }

}
















