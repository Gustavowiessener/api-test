package dataFactory;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class TestInPostVotesDogs {

    public static String token = "81687091-6c9b-4917-a11b-5bafff6bf156";


    public static String corpoRequest = "{\n" +
            "  \"image_id\": \"test-img\",\n" +
            "  \"sub_id\": \"meu-user-test\",\n" +
            "  \"value\": 10\n" +
            "}";

    public static String corpoError = "{\n" +
            "  \"image_id\": 20,\n" +
            "  \"sub_id\": \"test-error\",\n" +
            "  \"value\": 2\n" +
            "}";

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = "https://api.thedogapi.com/v1";
    }

    @Test
    @DisplayName("A Simulação de inserir dados na API deve retornar HTTP Status 200 com retorno do Body.")
    public void PostInVotesDog() {
        Response response = given()
                .header("x-api-key", token)
                .contentType(ContentType.JSON)
                .body(corpoRequest)
                .when()
                .post("/votes")
                .then()
                .log().all()
                .extract().response();


        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals("SUCCESS", response.jsonPath().getString("message"));
    }


    @Test
    @DisplayName("Dados inserior com erro/invalidos retornam HTTP status 400 e mensagem com informação.")
    public void PostInVotesDogError() {
        Response response = given()
                .header("x-api-key", token)
                .contentType(ContentType.JSON)
                .body(corpoError)
                .when()
                .post("/votes")
                .then()
                .log().all()
                .extract().response();


        Assertions.assertEquals(400, response.statusCode());
        Assertions.assertEquals("info", response.jsonPath().getString("level"));

    }
}