package dataFactory;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class TestInDeleteVotesDogs {

    public static String token = "81687091-6c9b-4917-a11b-5bafff6bf156";
    public static Integer id = 54044;

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = "https://api.thedogapi.com/v1/";
    }

    @Test
    @DisplayName("Dados excluidos deveram ter um Http status 200 e mensagem com a exclusão")
    public void DeleteInVotesDogs() {
        Response response = given()
                .header("x-api-key", token)
                .contentType(ContentType.JSON)
                .when()
                .delete("votes/" + id)
                .then()
                .log().all()
                .extract().response();


        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals("SUCCESS", response.jsonPath().getString("message"));

    }

}
