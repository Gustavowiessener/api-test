<h1>Teste de API - The dog</h1>

<span>Para inicialização do projeto, configure as variaveis de ambiente e configure o Path da aplicação e suas dependencias.</span>

<hr>
<span>Não se esqueça de <b>configurar o token de autentificação</b> do seu projeto, cada úsuario possui seu própio token.</span>
<h3>Documentação da API: </h3>
    <ul> <li>https://docs.thedogapi.com/api-reference/</li>

</ul>
<hr>

<span>Na raiz do projeto, através de seu Prompt de Commando/Terminal/Console execute o seguinte comando:</span>

<ul>
    <li>mvn test</li>
    <li>Para executar todos os testes em uma determinada c: <b>mvn test -Dtest=classname</b></li>
</ul>

<hr>

<h4>Dependências do projeto: </h4>

<span>Junit Jupiter version: 5.8.1</span>

<span>Rest Assured version: 4.4.0 </span>

<span>Apache Maven version 4.0.0 </span>

<hr>

<h3>Cenários :</h3>

<span>A Class <b>TestInGetDogs</b> tem a finalidade de gerir as requisições do tipo GET da aplicação: </span>
<h4>Não se esqueça de fazer as assertivas no corpo da requisição.</h4>

<ul> Cenários importantes:
    <li>[x] Ao solicitar uma listagem de todas as raças, retorno deve ser HTTP Status de 200 com a listagem.</li>
    <li>[x] Ao solicitar uma busca em determinada raça, o HTTP status deve ser 200, e a listagem de determinada raça, com o <b>id</b>, <b>nome</b> e <b>temperamento</b> do dog.</li>
</ul>

<hr>

<span>A Class <b>TestInVotesDogs</b> tem a finalidade de gerar as requisições para verificar os votos dos dogs da API, utilizaremos o método POST e GET nesta classe:</span>

<ul>Cenários importantes:
    <li>[x] Um votes criado com sucesso retorna HTTP Status 200, e os dados inseridos como retorno.</li>
    <li>[x] Uma simulação de votos com problema em alguma regra retorna o status 400.</li>
  
</ul>

<span>A Class <b>TestInDeleteVotesDogs</b> tem por finalidade gerar as requisições do tipo DELETE, simulando o delete dos votos:</span>

<ul>
    <li>[x] Retorna o HTTP status 204 se o vote for removido com sucesso.</li>
    <li>[x] Retorna o HTTP status 400 com mensagem se não encontrar. </li>
</ul>